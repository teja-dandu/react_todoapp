class Todo extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            todoList:[],
            response:""
            // heading:"",
            // check:"",

        };
    }

    render(){
        let bulletTodos = this.state.todoList.map((element, index) => {
            return (
                <li key={index}>{element}</li>
            );
        });

        return (
            <div>
                <h1>Todo App</h1>
                <input onKeyPress = {this.inputKeyPress} onChange={this.updateResponse}value={this.state.response} placeholder="add todo..."/>

                {this.state.todoList.length === 0 ? 'add some todos' : <ul>{bulletTodos}</ul>}
                {/*<div>
                    <button>All<button/>
                </div>*/}
            </div>
        );
    }
    updateResponse = (event) => {
        this.setState({
            response: event.target.value
        });
    }

    inputKeyPress = (event) => {
        if(event.key === "Enter"){
            let todos = this.state.todoList.slice();
            todos.push(this.state.response);
            this.setState({
                todoList: todos, response: ""
            });
        }
    }


}


class App extends React.Component {
    render(){
        return (
            <div>
                <Todo/>
            </div>

        );
    }
}

ReactDOM.render(<App/>, document.querySelector('#app'))
